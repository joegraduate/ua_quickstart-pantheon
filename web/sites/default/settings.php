<?php

/**
 * Configure Migrate module to always use Drush for import/rollback operations
 * to prevent timeout issues.
 *
 * @see https://pantheon.io/docs/timeouts/
 * @see https://www.drupal.org/node/1958170
 */
$conf['migrate_drush_path'] = $_ENV['HOME'] . '/drush';
$conf['migrate_import_method'] = 1;

/**
 * If there is a local settings file, then include it
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}
